# This file is part of Tryton.  The COPYRIGHT file at the top level of this
#repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import invoice


def register():
    Pool.register(
        invoice.Invoice,
        invoice.AddProductForm,
        module='invoice_editable_line', type_='model')
    Pool.register(
        invoice.WizardAddProduct,
        module='invoice_editable_line', type_='wizard')
